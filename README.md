# aws-projects

#1. Identify VPC ID
  aws ec2 describe-vpcs

#2. Create SG

  aws ec2 create-security-group \--group-name HelloWorld \--description "Hello World Demo" \--vpc-id vpc-4cddce2a

#3.  Open up SSH tcp/22 and tcp/3000

  aws ec2 authorize-security-group-ingress --group-name HelloWorld --protocol tcp --port 22 --cidr 0.0.0.0/0
  aws ec2 authorize-security-group-ingress --group-name HelloWorld --protocol tcp --port 3000 --cidr 0.0.0.0/0

#4. Create Key

  aws ec2 create-key-pair --key-name EffectiveDevOpsAWS --query  'KeyMaterial' --output text > ~/.ssh/EffectiveDevOpsAWS.pem
  chmod 400 ~/.ssh/EffectiveDevOpsAWS.pem
#5. Create EC2
  aws ec2 run-instances     --instance-type t2.micro     --key-name EffectiveDevOpsAWS     --security-group-ids sg-07dea2a2f759f2cea --image-id ami-0c2b8ca1dad447f8a

#6. Connect to EC2
  ssh ec2-user@54.160.103.104 -i ~/.ssh/EffectiveDevOpsAWS.pem

#7. Create NODE JS APP

  sudo yum install --enablerepo=epel -y nodejs
  node -v
#8. Run app
  node helloworld.js
  
  Check -> public-ip:3000

  Run in background -> sudo start helloworld
